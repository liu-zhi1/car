// 通用标准函数

var ajax_get_func=function(url,token,param,handle_func=""){
	url=url+"/?what="+param
    // console.log("get",url)
    $.ajax({
        url: url,
        headers:{"token":token},
        type: 'get',
        dataType: 'JSON',
        cache: false,
        success: function (v) {

            if(v.code==0){
            	var data=v.data

                if (handle_func!==""){
                    handle_func(data)

                }
            }
            if(v.msg!==""){
                layer.msg(v.msg)
            }
        },
        error:function(d){
            console.log(d)
        }
    });
}

var ajax_post_func=function(url,token,param,data,handle_func=""){
    url=url+"/?what="+param
    // console.log("post",url)
    $.ajax({
        url: url,
        headers:{"token":token},
        type: 'post',
        contentType: 'application/json',
        data:data,
        success: function (v) {
            // console.log(v)
            if(v.code==0){
                var data=v.data
                if (handle_func!==""){
                    handle_func(data)
                }

            }
            if(v.msg!==""){
                layer.msg(v.msg)
            }
            
        },
        error:function(d){
            console.log(d)
        }
    });
}

// 数组中获取服务查询条件的单个
var query_detail = function(d,f,k){
    var res;
    // console.log("query_detail-->",d,f,k)
    d.forEach(item=>{

        if(item[f]==k){
            // console.log("query_detail-->",item,f,k)
            res=item
        }
    })
    return res
}
// 表格模糊查询
var query_key_list = ['created_at',"updated_at","scan_info_rec_ts"]
var query_func = function(d,k){
    var res_list = []
    if (k == ""){

        return d
    }

    d.forEach(item=>{
        var r = false
        for(var n in item){
            if (query_key_list.indexOf(n)!=-1){
                continue
            }
            var tar = item[n]
            if(tar.length > 1 && tar.length<30){

                // console.log("tar,tar.length",tar,tar.length)
                if(tar.indexOf(k)!=-1){
                    r = true
                    break
                }
            }
            
        }
        if(r){
            res_list.push(item)
        }
    })
    return res_list
}


// 搜索功能
var search_func=function(tagret_data,search_key,...target_field){
  // console.log(tagret_data,search_key,target_field1,target_field2)
  result_data=[]
  for(var i=0;i<tagret_data.length;i++){
    for(var n = 0;n < target_field.length;n++){
        if (tagret_data[i][target_field[n]] == null){
            continue
        }
        if (tagret_data[i][target_field[n]].indexOf(search_key) != -1){
            result_data.push(tagret_data[i])
            break
        }
    }
  }
  return result_data
}



// 通过设备id获取设备具体信息
var get_detail_from_id=function(data,device_id,field='device_id'){
  for(var i=0;i<data.length;i++){
    if(data[i][field]==device_id){
      return data[i]
    }
  }
}

// 图片上传功能
var img_upload_func=function(div_id){
  upload.render({
  elem: '#'+div_id
  ,auto: false
  ,choose: function(obj){
    obj.preview(function(index, file, result){
      // result是图片 base64编码
      if (result.length>8000000){
        layer.msg("图片不能超过5MB")
        return false
      }else{
        $('#'+div_id).attr('src',result)
      }
      
    });
  }
});
}

// 设备单选框填充
var fill_raido_func=function(fill_data,list_id,title,key='device_id'){
    
  $('#'+list_id).empty()
  if(title=='none'){
    for (var i = fill_data.length - 1; i >= 0; i--) {
      var option='<option>'+fill_data[i][key]+'</option>'
      $('#'+list_id).append(option)
    }
  }else{
    for (var i = fill_data.length - 1; i >= 0; i--) {
      var option='<option value="'+fill_data[i][key]+'">'+fill_data[i][title]+'</option>'

      if(fill_data[i][key]==chosed_dev.device_id){
        option='<option value="'+fill_data[i][key]+'" selected>'+fill_data[i][title]+'</option>'
      }
      $('#'+list_id).append(option)
    }
  }
  
}

// 筛选申请信息
var filter_func=function(data,field,key,method=''){
  var res=[]
  if(method=='不等'){
    for(var i=0;i<data.length;i++){
      if(data[i][field]!==key){
        res.push(data[i])
      }
    }
  }else{
    for(var i=0;i<data.length;i++){
      if(data[i][field]==key){
        res.push(data[i])
      }
    }
  }
  return res
}

// 填充信息
var fill_option=function(fill_data,list_id,key,title=''){
  $('#'+list_id).empty()
  if(title==''){
    for (var i = fill_data.length - 1; i >= 0; i--) {
      var option='<option>'+fill_data[i][key]+'</option>'
      $('#'+list_id).append(option)
    }
  }else{
    for (var i = fill_data.length - 1; i >= 0; i--) {
      var option='<option value="'+fill_data[i][key]+'">'+fill_data[i][title]+'</option>'
      $('#'+list_id).append(option)
    }
  }
}

// 时间戳转时间字符
var formatDate = function (date) {
    var date = new Date(date);
    var YY = date.getFullYear() + '-';
    var MM = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    var DD = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate());
    var hh = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
    var mm = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
    var ss = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
    return YY + MM + DD +" "+hh + mm + ss;
}


// #wgs84坐标系转换成百度坐标系计算函数
function wgs84_bdmap(input_lat,input_lng){

    var pi = 3.1415926535897932384626;
    var a = 6378245.0;
    var ee = 0.00669342162296594323;
    var x_pi = 3.14159265358979324 * 3000.0 / 180.0;


    function wgs2bd(lat, lon){ 
           _wgs2gcj = wgs2gcj(lat, lon);
           _gcj2bd = gcj2bd(_wgs2gcj[0], _wgs2gcj[1]);
           return _gcj2bd;
       }
    

    function gcj2bd(lat, lon){
           x = lon
           y = lat
           z = Math.sqrt(x * x + y * y) + 0.00002 * Math.sin(y * x_pi);
           theta = Math.atan2(y, x) + 0.000003 * Math.cos(x * x_pi);
           bd_lon = z * Math.cos(theta) + 0.0065;
           bd_lat = z * Math.sin(theta) + 0.006;
           return [ bd_lat, bd_lon ];
       }
    

    function bd2gcj(lat, lon){ 
           x = lon - 0.0065
           y = lat - 0.006
           z = Math.sqrt(x * x + y * y) - 0.00002 * Math.sin(y * x_pi);
           theta = Math.atan2(y, x) - 0.000003 * Math.cos(x * x_pi);
           gg_lon = z * Math.cos(theta);
           gg_lat = z * Math.sin(theta);
           return [ gg_lat, gg_lon ];
       }
    

    function wgs2gcj(lat, lon){ 
           dLat = transformLat(lon - 105.0, lat - 35.0);
           dLon = transformLon(lon - 105.0, lat - 35.0);
           radLat = lat / 180.0 * pi;
           magic = Math.sin(radLat);
           magic = 1 - ee * magic * magic;
           sqrtMagic = Math.sqrt(magic);
           dLat = (dLat * 180.0) / ((a * (1 - ee)) / (magic * sqrtMagic) * pi);
           dLon = (dLon * 180.0) / (a / sqrtMagic * Math.cos(radLat) * pi);
           mgLat = lat + dLat;
           mgLon = lon + dLon;
           return [ mgLat, mgLon ]
       }
    

    function transformLat(lat, lon){ 
           ret = -100.0 + 2.0 * lat + 3.0 * lon + 0.2 * lon * lon + 0.1 * lat * lon + 0.2 * Math.sqrt(Math.abs(lat));
           ret += (20.0 * Math.sin(6.0 * lat * pi) + 20.0 * Math.sin(2.0 * lat * pi)) * 2.0 / 3.0;
           ret += (20.0 * Math.sin(lon * pi) + 40.0 * Math.sin(lon / 3.0 * pi)) * 2.0 / 3.0;
           ret += (160.0 * Math.sin(lon / 12.0 * pi) + 320 * Math.sin(lon * pi  / 30.0)) * 2.0 / 3.0;
           return ret;
       }
    

    function transformLon(lat, lon){ 
           ret = 300.0 + lat + 2.0 * lon + 0.1 * lat * lat + 0.1 * lat * lon + 0.1 * Math.sqrt(Math.abs(lat));
           ret += (20.0 * Math.sin(6.0 * lat * pi) + 20.0 * Math.sin(2.0 * lat * pi)) * 2.0 / 3.0;
           ret += (20.0 * Math.sin(lat * pi) + 40.0 * Math.sin(lat / 3.0 * pi)) * 2.0 / 3.0;
           ret += (150.0 * Math.sin(lat / 12.0 * pi) + 300.0 * Math.sin(lat / 30.0 * pi)) * 2.0 / 3.0;
           return ret;
        }

    wgs2bd=wgs2bd(input_lat,input_lng)
    wgs2bd.reverse()
    return wgs2bd

}




