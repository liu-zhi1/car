#usr/bin/env python
#conding:utf-8
# 吕康宁，20201222
# 定位器系统数据库表格设计

from django.db import models
from django.contrib.auth.models import User
import datetime

# Create your models here.

class Device(models.Model): 
	device_id=models.CharField('设备ID号',max_length=100,unique=True)
	auth_id=models.CharField('绑定授权码',max_length=100,default=0)
	iccid=models.CharField('通讯卡号',max_length=100,default=0)
	model=models.CharField('设备型号',max_length=100,default=0)
	user_name=models.CharField('车牌/人名',max_length=100,default=0)
	user_detail=models.TextField('车/人详情',default=0)
	dynamic=models.TextField('实时信息',default=0)
	param_set=models.TextField('参数信息',default=0)
	efence=models.TextField('围栏',default=0)
	polyline=models.TextField('路线',default=0)
	status=models.CharField('任务状态',max_length=200,default=0)
	remark=models.CharField('备注',max_length=200,default=0)
	date_added=models.CharField('添加时间',max_length=100,default=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
	owner=models.CharField('所属用户',max_length=500,default='admin')

class Car(models.Model):#车辆基础信息录入表
	name=models.CharField('车牌',max_length=50,unique=True)
	detail=models.TextField('车辆详情',default=0)
	param_set=models.TextField('参数信息',default=0)
	status=models.TextField('任务状态',default=0)
	device_id=models.CharField('绑定设备ID',max_length=50,default=0)
	date_added=models.CharField('操作时间',max_length=50,default=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
	remark=models.CharField('备注',max_length=50,default=0)
	owner=models.CharField('所属用户',max_length=500,default='admin')

class Car_Apply(models.Model):#车辆基础信息录入表
	start=models.CharField('出发地',max_length=150,default=0)
	end=models.CharField('目的地',max_length=150,default=0)
	purpose=models.CharField('用车事由',max_length=50,default=0)
	passenger=models.CharField('乘车人数',max_length=50,default=0)
	time_use=models.CharField('用车时长',max_length=100,default=0)
	param_set=models.TextField('参数信息',default=0)
	status=models.TextField('状态信息',default=0)
	car=models.CharField('申请车辆',max_length=50,default=0)
	apply_time=models.CharField('申请时间',max_length=50,default=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
	owner=models.CharField('申请人',max_length=50,default=0)
	remark=models.CharField('备注',max_length=50,default=0)

class Person(models.Model):#人员基础信息录入表
	name=models.CharField('姓名',max_length=50,unique=True)
	detail=models.TextField('人员详情',default=0)
	param_set=models.TextField('参数信息',default=0)
	status=models.TextField('任务状态',default=0)
	device_id=models.CharField('绑定设备ID',max_length=50,default=0)
	date_added=models.CharField('操作时间',max_length=50,default=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
	remark=models.CharField('备注',max_length=50,default=0)
	owner=models.CharField('所属用户',max_length=500,default='admin')

class Building(models.Model): #车间基础信息录入表
	name=models.CharField('建筑名称',max_length=50,unique=True)
	name_id=models.CharField('建筑编号',max_length=50,default=0)
	position=models.CharField('建筑位置坐标',max_length=300,default=0)
	scanner=models.TextField('基站设备',default=0)
	camera=models.TextField('摄像头',default=0)
	param_set=models.TextField('参数信息',default=0)#人数，进入时限等
	asset=models.TextField('资产信息',default=0)
	othermesg=models.CharField('其他信息',max_length=50,default=0)
	kind=models.CharField('建筑类型',max_length=50,default=0)
	remark=models.CharField('备注',max_length=50,default=0)
	date_added=models.CharField('添加时间',max_length=50,default=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
	owner=models.CharField('所属用户',max_length=500,default='admin')

class Scanner(models.Model):#定位基站信息录入表
	device_id=models.CharField('设备ID',max_length=50,unique=True)
	name=models.CharField('设备名称',max_length=50,default=0)
	model=models.CharField('设备型号',max_length=50,default=0)
	position=models.CharField('安装位置',max_length=200,default=0)
	scanner_detail=models.TextField('基站详情',default=0)
	param_set=models.TextField('参数信息',default=0)
	kind=models.CharField('基站类别',max_length=50,default=0)
	status=models.CharField('设备状态',max_length=50,default=0)
	show_icon=models.TextField('显示图标',default=0)
	scann_data=models.TextField('扫描数据',default=0)
	remark=models.CharField('备注',max_length=50,default=0)
	date_added=models.CharField('添加时间',max_length=50,default=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
	owner=models.owner=models.CharField('所属用户',max_length=500,default='admin')

class Efence(models.Model):
	name=models.CharField('围栏名称',max_length=50,unique=True)
	kind=models.CharField('围栏类型',max_length=50,default=0)
	content=models.TextField('围栏内容',default=0)
	e_time=models.CharField('生效时间',max_length=50,default=0)
	s_time=models.CharField('结束时间',max_length=50,default=0)
	alarm_kind=models.CharField('报警类型',max_length=150,default=0)
	device_id=models.TextField('绑定设备ID',default=0)
	date_added=models.CharField('操作时间',max_length=50,default=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
	remark=models.CharField('备注',max_length=50,default=0)
	owner=models.owner=models.CharField('所属用户',max_length=500,default='admin')

class Alarm(models.Model):
	name=models.CharField('报警编号',max_length=200,default=0)
	device_id=models.CharField('报警设备',max_length=50)
	location=models.TextField('报警地点',default=0)
	content=models.CharField('报警内容',max_length=500,default=0)
	kind=models.CharField('报警类型',max_length=500,default=0)
	s_time=models.CharField('报警开始时间',max_length=100)
	e_time=models.CharField('报警开始时间',max_length=100)
	handle=models.CharField('报警处理',max_length=50,default='no')
	remark=models.CharField('备注',max_length=50,default=0)
	owner=models.owner=models.CharField('所属用户',max_length=500,default='admin')

class Myuser(models.Model):
	username=models.CharField('登录名',max_length=100,unique=True)
	visible_name=models.CharField('展示名称',max_length=100,default=0)
	phone=models.CharField('电话',max_length=100,default=0)
	leader=models.CharField('所属上级',max_length=100,default=0)
	subordinate=models.TextField('下级用户',default=0)
	kind=models.CharField('用户类型',max_length=100,default=0)
	character=models.TextField('角色',default=0)
	status=models.CharField('状态',max_length=100,default='on')
	remark=models.CharField('备注',max_length=100,default=0)
	date_added=models.CharField('操作时间',max_length=500,default=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

class Multi_Media(models.Model):
	send_time=models.CharField('发送时间',max_length=50,default=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
	sender=models.CharField('发送方',max_length=500,null=False)
	receiver=models.CharField('接收方',max_length=500,null=False)
	content=models.TextField('信息内容',default=0)
	content_kind=models.CharField('信息类型',max_length=50,default=0)
	location=models.CharField('发送地点',max_length=500,default=0)
	owner=models.owner=models.CharField('所属用户',max_length=500,default='admin')

class WIFI_LBS_ADDR(models.Model):
	mac=models.CharField('mac地址',max_length=100,unique=True)
	ssid=models.CharField('wifi名称',max_length=100,default=0)
	sid=models.CharField('基站区域码',max_length=100,default=0)
	nid=models.CharField('基站区域码',max_length=100,default=0)
	mcc=models.CharField('国家码',max_length=100,default=0)
	mnc=models.CharField('网号',max_length=100,default=0)
	rssi=models.CharField('信号强度',max_length=100,default=0)
	location=models.TextField('信息内容',default=0)
	lng=models.CharField('经度',max_length=100,default=0)
	lat=models.CharField('纬度',max_length=100,default=0)
	kind=models.CharField('类型',max_length=100,default=0)
	date_added=models.CharField('添加时间',max_length=50,default=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

class Map_Data(models.Model):
	name=models.CharField('名称',max_length=50,unique=True)
	name_id=models.CharField('标识ID',max_length=50,default=0)
	kind=models.CharField('地图类型',max_length=50,default=0)
	content = models.TextField('地图内容',default=0)
	overlay=models.TextField('覆盖物',default=0)
	param_set=models.TextField('地图参数',default=0)
	date_added=models.CharField('操作时间',max_length=100,default=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
	remark=models.CharField('备注',max_length=50,default=0)
	owner=models.owner=models.CharField('所属用户',max_length=500,default='admin')

class Command_Log(models.Model):
	command_id=models.CharField('命令ID',max_length=50,unique=True)
	device_id=models.CharField('设备ID',max_length=50,default=0)
	command_name=models.CharField('命令名',max_length=50,default=0)
	command_style=models.CharField('命令格式',max_length=50,default=0)
	command_value = models.TextField('命令参数',default=0)
	command_result=models.CharField('命令执行结果',max_length=50,default=0)
	send_time=models.CharField('发送时间',max_length=100,default=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
	remark=models.CharField('备注',max_length=50,default=0)
	owner=models.owner=models.CharField('所属用户',max_length=500,default='admin')
	
class Sys_Log(models.Model):
	username=models.CharField('登录名',max_length=100,default=0)
	visible_name=models.CharField('展示名称',max_length=100,default=0)
	content=models.TextField('操作内容',max_length=100,default=0)
	result=models.CharField('执行结果',max_length=100,default='成功')
	remark=models.CharField('备注',max_length=100,default=0)
	date_added=models.CharField('操作时间',max_length=100,default=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
	owner=models.owner=models.CharField('所属用户',max_length=50,default='admin')

class Daily_Report(models.Model):
	report_name=models.CharField('名称',max_length=100,unique=True)
	device_id=models.CharField('设备ID',max_length=100)
	analysis_time=models.CharField('统计时间',max_length=100)
	active=models.CharField('活跃',max_length=100,default=0)
	online=models.CharField('在线',max_length=100,default=0)
	alarm_time=models.CharField('报警时段',max_length=500,default=0) #每小时一个时段，即0-1,1-2，一天24时段，统计报警出现的高峰时段
	alarm_kind=models.CharField('报警种类',max_length=500,default=0)
	alarm_location=models.TextField('报警地段',default=0) #以经纬度的相似为范围，统计报警出现的高峰地段，范围大的时候数据量太大，不需要统计到首页报表
	heart_rate=models.CharField('心率',max_length=100,default=0)
	blood_pres=models.CharField('血压',max_length=100,default=0)
	step_count=models.CharField('步数',max_length=100,default=0)
	mileage=models.CharField('GPS里程',max_length=100,default=0)
	calorie=models.CharField('卡路里消耗',max_length=100,default=0)
	stop_time=models.CharField('静止',max_length=100,default=0)
	sleep=models.CharField('睡眠',max_length=500,default=0)
	sensor_analysis=models.TextField('其他传感器参数',default=0)
	attch_analysis=models.TextField('附属分析数据',default=0) #例如上述已统计的在线，活跃，睡眠等的数据依据描述，例如时间范围，数据条数，有效数据，权重设置等，人员身体数据的性别，年龄、身高，体重等获取与权重。
	remark=models.CharField('备注',max_length=100,default=0)
	create_time=models.CharField('创建时间',max_length=100,default=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))


