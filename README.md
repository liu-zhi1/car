# gps车辆定位管理系统

#### 介绍
通过给车辆安装gps定位器，实现车辆的监控管理

#### 软件架构
软件架构说明


1. python，开发语言
1. django ，web框架
1. twisted，数据接收使用的框架
1. 数据库：postgresql，最新更改为sqlite，减少部署发麻烦
1. 前端：layui
1. 中间件：redis，连接数据twisted和django,传递数据

#### 安装和使用教程

1. python manage.py createsuperuser, 用户名为：admin,密码随意设置
1. python manage.py runserver 有缺少的库，参照requirements.txt，网页输入127.0.0.1:8000可以看到运行结果
1. python datahandle/receive_serv.py，独立运行,功能：数据接收，并解析，解析出来时候放入redis，设备的监听是10808，默认协议是jt808部标通讯协议，目前也只解析了定位和一些基本参数。
1. python datahandle/store_serv.py，独立运行,功能：从redis接受解析好的数据，进行状态计算（超速报警，围栏报警等）和存储
1. 


### car_v2_pub 发布，为golang重构，部署简便，具体看部署文档，欢迎尝试(此版本闭源，适配有微信小程序和安卓app，有需要联系作者)
体验登录：
http://szjianyitong.com/
体验账号密码：demo,123456

微调之后，可以直接用于小型车队的调度管理。

最后放些效果图
![输入图片说明](%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20231102090231.png)
![输入图片说明](%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20231102093630.png)
![输入图片说明](%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20231102093651.png)
![输入图片说明](%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20231102093807.png)
![输入图片说明](%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20231102101624.png)
![输入图片说明](%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20231102104049.png)

![输入图片说明](%E9%A6%96%E9%A1%B5.jpg)
![输入图片说明](%E7%94%A8%E8%BD%A6%E7%94%B3%E8%AF%B7%E8%A1%A8.jpg)
![输入图片说明](%E7%94%B3%E8%AF%B7%E7%94%A8%E8%BD%A6.jpg)
![输入图片说明](%E5%9C%B0%E5%9B%BE%E6%9F%A5%E8%BD%A6%20(2).jpg)
![输入图片说明](%E8%BD%A8%E8%BF%B9%E6%92%AD%E6%94%BE.jpg)


### 交流+V：

![输入图片说明](%E4%B8%AA%E4%BA%BA%E5%BE%AE%E4%BF%A1.jpg)




