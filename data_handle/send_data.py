# -*- coding: utf-8 -*-
# @Author  : 陈和权
# @Time    : 2019/10/19 16:08
# @File    : jt808_command.py
import binascii
import socket,random

# sd=b'gg\x01\x00\x0b\x00\x04\x08b\x96@!\x032W\x00'
# sd=b'[3G*9613051350*0010*LK,31537,5043,28]'
ded=['123464','123467','123459','123468','8888888','123461','123469','110','67890','9613051350']
sdlist=[]

for x in ded:
	# lng=random.uniform(114.443669,115.443669)
	# lat=random.uniform(22.822995,23.822995)
	heart=random.randint(80,150)
	lat=random.randint(80,150)
	# sd= b'[3G*'+x.encode()+b'*00FA*UD_LTE,180620,061023,A,'+str(lng).encode()+b',N,'+str(lat).encode()+b',E,22.0,0,-1,19,100,46,0,0,00000000,1,1,460,11,46096,188786183,100,5,lknwifi,7e:03:ab:fb:8e:4d,-42,LGYYNW18F,70:6d:15:ed:da:51,-71,CMS18,30:b4:9e:e1:8e:00,-81,CMS18,30:b4:9e:e1:88:00,-82,CMS18,30:b4:9e:de:e6:b0,-82,0.0]'
	sd='[3G*'+x+'*000F*bphrt,'+str(lat)+','+str(lat)+','+str(lat)+']'
	sdlist.append(sd)
# print(sdlist)


# def set_param(sd):
# 	ip_port = ('localhost',10707)
# 	sk = socket.socket()
# 	sk.connect(ip_port)
# 	sk.send(sd)
# 	server_reply='no_replay'
# 	print(server_reply)
# 	sk.close()

# for x in range(len(sdlist)):
# 	print(x)
# 	set_param(sdlist[x])

import threading
import time



class myThread (threading.Thread):
	def __init__(self,name,idnum):
		threading.Thread.__init__(self)
		self.name = name
		self.idnum=idnum
	def run(self):
		print ("开始线程：" + str(self.idnum))
		print_time(self.name)
		print ("退出线程：" + str(self.idnum))

def print_time(sd):
	print('type------------',type(sd))
	sd=sd.encode()
	ip_port = ('localhost',10707)
	sk = socket.socket()
	sk.connect(ip_port)
	sk.send(sd)
	server_reply='no_replay'
	print(server_reply)

n=0
for x in sdlist:
	thread1 = myThread(x,n)
	thread1.start()
	n+=1
